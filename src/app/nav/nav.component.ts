import { Component, OnInit } from '@angular/core';
import { Apollo } from "apollo-angular";
import gql from "graphql-tag";
import { CategoryListQuery } from "../apollo/queries/category/categories";

import { Subscription } from "rxjs";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  data: any = {};
  loading = true;
  errors: any;

  private queryCategories: Subscription;

  constructor(
    private apollo: Apollo) {

  }

  ngOnInit(): void {
    this.queryCategories = this.apollo
      .watchQuery({
        query: new CategoryListQuery().query()
      })
      .valueChanges.subscribe(result => {
        this.data = result.data;
        this.loading = result.loading;
        this.errors = result.errors;

        console.log(this.errors);
        console.log(this.data);
        console.log(this.loading);
      });
  }

  ngOnDestroy() {
    this.queryCategories.unsubscribe();
  }

}
