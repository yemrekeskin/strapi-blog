import { DocumentNode } from "graphql";
import gql from "graphql-tag";

export class ArticleListQuery {

  public query = (): DocumentNode => {
    return gql`
      query Articles {
        articles {
          id
          title
          category {
            id
            name
          }
          image {
            url
          }
        }
      }`;
  }

  public getQuery = (): DocumentNode => {
    return gql`
    query Articles($id: ID!) {
      article(id: $id) {
        id
        title
        content
        image {
          url
        }
        category {
          id
          name
        }
        published_at
      }
    }`;
  }

  public getArticlesByCategoryQuery = (): DocumentNode => {
    return gql`
    query Category($id: ID!) {
      category(id: $id) {
        id
        name
        articles {
          id
          title
          content
          image {
            url
          }
          category {
            id
            name
          }
        }
      }
    }`;
  }
}
