import { DocumentNode } from "graphql";
import gql from "graphql-tag";

export class CategoryListQuery {

  public query = (): DocumentNode => {
    return gql`
      query Categories {
        categories {
          id
          name
        }
      }`;
  }
}
